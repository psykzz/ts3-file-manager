﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
// TS3 Query Lib
using TS3QueryLib.Core;
using TS3QueryLib.Core.Query;
using TS3QueryLib.Core.Query.Responses;

namespace ts3_file_manager
{
    public partial class loginForm : Form
    {
        public loginForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Connect())
            {
                Hide();
                Application.Run(new mainForm());
            } else {
                MessageBox.Show("unable to connect...");
            }
        }
        bool Connect()
        {
            string targetHost = txtAddress.Text; // the IP or Hostname of the teamspeak server to connect to
            ushort targetQueryPort = ushort.Parse(txtPort.Text); // the query port of the teamspeak server to connect to
            string login = "serveradmin"; // the username used in Login-Method
            string password = "ANicePassword"; // the password for the login

            // The QueryRunner is used to send queries. All Queries are implemented type save and return objects with properties
            using (QueryRunner queryRunner = new QueryRunner(new SyncTcpDispatcher(targetHost, targetQueryPort))) // host and port
            {
                // connection to the TS3-Server is established with the first query command when using SyncTcpDispatcher

                // login using the provided username and password and show a dump-output of the response in a textbox
                SimpleResponse loginResponse = queryRunner.Login(login, password);

                if (loginResponse.IsErroneous)
                {
                    // login failed! Use loginResponse.ErrorMessage and loginResponse.ErrorId to get the reason
                    return false;
                }

                VersionResponse versionResponse = queryRunner.GetVersion();

                if (!versionResponse.IsErroneous)
                {
                    // use versionResponse.Platform, versionResponse.Version and versionResponse.Build to show the version info of the server
                }
            }
            return true;
        }
    }
}
